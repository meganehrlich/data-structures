# Data Structures Practice

There are a variety of data structures in these problems
that let you build data structures using test-driven
development one step at a time so that it's not
overwhelming.

* [Linked Queue](./linked_queue/README.md)
* [Linked List](./linked_list/README.md)
* [Circular Queue](./circular_queue/README.md)

## More links
For linked lists:
   https://www.geeksforgeeks.org/data-structures/linked-list/

For linked queues:
   https://www.geeksforgeeks.org/queue-linked-list-implementation/